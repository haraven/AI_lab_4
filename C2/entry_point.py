import sys

from Interface.Menu import Menu, MenuItem
from expert_systems.certain.CertainSystem import CertainSystem
from expert_systems.fuzzy.FuzzySystem import FuzzySystem
from utils.FactUtils import FactReader


def setup_main_menu(my_menu):
    if not issubclass(my_menu.__class__, Menu):
        raise TypeError("Cannot initialize " + my_menu + " as it is not a recognized menu.")

    item = Menu()
    item.append(MenuItem("- choose an input file for the facts(file);"))
    my_menu.append(item)
    item = MenuItem("- solve;")
    my_menu.append(item)
    item = MenuItem("- exit.")
    my_menu.append(item)

    return my_menu


def handle_input(main_menu):
    print("Welcome. Please choose one of the below options: ")
    while True:
        try:
            main_menu.parse()
            cmd = input("> ")
            if cmd == "file":
                print("\t a facts input file must be of the form:"
                      "\n\t\t\033[91mfact1"
                      "\n\t\tfact2"
                      "\n\t\t..."
                      "\n\t\tfactn\033[0m"
                      "\n\twhere a fact can be: generic (e.g. 'summer', or 'the wind is blowing'),"
                      "or specific (e.g. 'temperature: 23', or 'days: 3')")
                filename = input("Enter a file to read from:\n> ")
                facts = FactReader.read_facts_as_array(filename)
            elif cmd == "solve":
                if facts is None:
                    raise SyntaxError(
                        "No facts have been set. You must initialize the facts before attempting to solve the problem.")

                res = CertainSystem.compute_uptime_forward_inf(facts)
                print("-----------------------------------------------")
                print("Certain system")
                print("-----------------------------------------------")
                print("Using forward inference, it has been found that the sprinkler will run for: " + str(
                    res) + " minutes.")
                res = CertainSystem.compute_uptime_backward_inf("sprinkle for 10 minutes")
                fixed_res = res[:(len(res) - 2)]
                print("Using backwards inference, the following facts have been found: " + fixed_res + "\n")

                print("-----------------------------------------------")
                print("Fuzzy system")
                print("-----------------------------------------------")
                temp = int(input("Enter the current temperature:\n> "))
                humidity = int(input("Enter the current humidity:\n> "))
                res = FuzzySystem.estimated_usage_time(temp, humidity)
                print("Using fuzzy logic, it has been found that the sprinkler will run for " + str(
                    res) + " minutes.\n")
            elif cmd == "exit":
                print("The application will now exit. Have a good one!")
                sys.exit(0)
            else:
                raise SyntaxError("Invalid command")
        except Exception as e:
            print("Unexpected error: " + str(e))


if __name__ == "__main__":
    menu = setup_main_menu(Menu())
    handle_input(menu)
