class FactReader:
    @staticmethod
    def read_facts_as_array(filename):
        facts = []
        file = open(filename)
        for line in file:
            facts.append(line)

        return facts
