from math import inf


class CertainSystem:
    @staticmethod
    def compute_uptime_forward_inf(facts):
        uptime = 0

        should_function = True
        crt_humidity = ""
        crt_temp = ""
        crt_humidity_int = inf
        crt_temp_int = inf
        day_count = 1

        for fact in facts:
            if fact == "clear sky":
                crt_humidity = "< 50"
            if fact == "summer":
                crt_temp = "> 20"
            if fact == "winter":
                crt_temp = "< 5"
                should_function = False
            if fact == "cloudy":
                crt_humidity = "> 60"
            if "temperature" in fact:
                fact_split = fact.split(": ")
                crt_temp_int = int(fact_split[1])
            if "humidity" in fact:
                fact_split = fact.split(": ")
                crt_humidity_int = int(fact_split[1])
            if "days" in fact:
                fact_split = fact.split(": ")
                day_count = int(fact_split[1])

            if crt_temp_int != inf:
                if crt_temp_int > 40:
                    uptime = 30
            elif crt_temp > "> 40":
                uptime = 30

            if crt_temp_int != inf:
                if crt_humidity_int != inf:
                    if crt_temp_int > 25 and crt_humidity_int < 50:
                        uptime = 20
                    elif crt_temp_int > 25 and crt_humidity <= "< 50":
                        uptime = 20
            else:
                if crt_humidity_int != inf:
                    if crt_temp > "> 25" and crt_humidity < 50:
                        uptime = 20
                elif crt_temp > "> 25" and crt_humidity < "< 50":
                    uptime = 20

            if crt_temp_int != inf:
                if 15 < crt_temp_int < 25 and crt_humidity <= "< 50":
                    uptime = 10

            if crt_temp_int != inf:
                if 15 < crt_temp_int < 25 and crt_humidity >= "> 50":
                    uptime = 3

        if not should_function:
            return 0
        return day_count * uptime

    @staticmethod
    def compute_uptime_backward_inf(fact):
        res = ""
        temp = "N/A"
        humidity = "N/A"
        rules = []
        rule_applied = True

        while rule_applied:
            rule_applied = False
            if fact == "sprinkle for 30 minutes" and 1 not in rules:
                temp = "> 40"
                rules.append(1)
                rule_applied = True
            if humidity <= "< 50" and humidity[0] == '<' and 2 not in rules:
                res += "clear sky, "
                rules.append(2)
                rule_applied = True
            if fact == "sprinkle for 20 minutes" and 3 not in rules:
                temp = "> 25"
                humidity = "< 50"
                rules.append(3)
                rule_applied = True
            if temp >= "> 20" and temp[0] == '>' and 4 not in rules:
                res += "summer, "
                rules.append(4)
                rule_applied = True
            if temp <= "< 5" and temp[0] == '<' and 5 not in rules:
                res += "summer, "
                rules.append(5)
                rule_applied = True
            if humidity >= "> 60" and humidity[0] == '>' and 6 not in rules:
                res += "cloudy, "
                rules.append(6)
                rule_applied = True
            if fact == "sprinkle for 10 minutes" and 7 not in rules:
                humidity = "< 50"
                temp = "15 < ; < 25"
                res += "hot and hasn't rained in a while, "
                rules.append(7)
                rule_applied = True
            if fact == "sprinkle for 3 minutes" and 8 not in rules:
                humidity = "> 50"
                temp = "15 < ; < 25"
                res += "hot and has rained recently, "
                rules.append(8)
                rule_applied = True
            if fact == "not functioning" and 9 not in rules:
                res += "winter, "
                rules.append(9)
                rule_applied = True

            fact = ""

        return res
