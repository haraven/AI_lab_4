from expert_systems.rules.RuleBase import RuleBase


class ComparisonRule(RuleBase):

    def __init__(self, operator, value):
        self.__operator = operator
        if type(value) is str:
            self.__value = int(value)
        else:
            self.__value = value

    def get_operator(self):
        return self.__operator

    def get_value(self):
        return self.__value

    def compare_to(self, rule):
        if rule.__value > self.__value:
            return 1
        if rule.__value == self.__value:
            return 0

        return -1

    def set_operator(self, operator):
        self.__operator = operator

    def set_value(self, value):
        if type(value) is str:
            self.__value = int(value)
        else:
            self.__value = value
