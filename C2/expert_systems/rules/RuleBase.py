from abc import ABCMeta, abstractmethod


class RuleBase(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_operator(self):
        pass

    @abstractmethod
    def get_value(self):
        pass

    @abstractmethod
    def compare_to(self, rule):
        pass

    @abstractmethod
    def set_operator(self, operator):
        pass

    @abstractmethod
    def set_value(self, value):
        pass
