class FuzzySystem:
    @staticmethod
    def compute_temp_freezing(temp):
        if -20.0 < temp < 5.0:
            return (5.0 - temp) / 25
        if temp < -20.0:
            return 1.0

        return 0.0

    @staticmethod
    def compute_temp_cold(temp):
        if -5.0 < temp <= 5.0:
            return (temp + 5) / 10
        if 5.0 < temp < 10:
            return (10 - temp) / 5

        return 0.0

    @staticmethod
    def compute_temp_normal(temp):
        if 5.0 <= temp <= 20:
            return (temp - 5) / 5
        if 10 < temp < 15:
            return 1.0
        if 15 <= temp < 20:
            return (20 - temp) / 5

        return 0.0

    @staticmethod
    def compute_temp_warm(temp):
        if 15 < temp <= 20:
            return (temp - 5) / 5
        if 20 < temp < 25:
            return (25 - temp) / 5

        return 0.0

    @staticmethod
    def compute_temp_hot(temp):
        if 25 <= temp <= 30:
            return (temp - 25) / 5
        if temp > 30:
            return 1

        return 0.0

    @staticmethod
    def compute_humidity_dry(humidity):
        if 0 <= humidity < 50:
            return humidity / 50

        return 0.0

    @staticmethod
    def compute_humidity_normal(humidity):
        if 0 < humidity <= 50:
            return humidity / 50
        if 50 < humidity < 100:
            return 100 - humidity / 50

        return 0.0

    @staticmethod
    def compute_humidity_humid(humidity):
        if 50 < humidity < 100:
            return (100 - humidity) / 50

        return 0.0

    @staticmethod
    def compute_uptime_short(uptime):
        if 0 <= uptime < 50:
            return uptime / 50

        return 0.0

    @staticmethod
    def compute_uptime_medium(uptime):
        if 0 < uptime <= 50:
            return uptime / 50
        if 50 < uptime < 100:
            return (100 - uptime) / 50

        return 0.0

    @staticmethod
    def compute_uptime_long(uptime):
        if 50 < uptime < 100:
            return (100 - uptime) / 50

        return 0.0

    @staticmethod
    def estimated_usage_time(temp, humidity):
        temp_freezing = FuzzySystem.compute_temp_freezing(temp)
        temp_cold = FuzzySystem.compute_temp_cold(temp)
        temp_normal = FuzzySystem.compute_temp_normal(temp)
        temp_warm = FuzzySystem.compute_temp_warm(temp)
        temp_hot = FuzzySystem.compute_temp_hot(temp)

        humidity_dry = FuzzySystem.compute_humidity_dry(humidity)
        humidity_normal = FuzzySystem.compute_humidity_normal(humidity)
        humidity_humid = FuzzySystem.compute_humidity_humid(humidity)

        max_uptime_short = min(temp_freezing, humidity_humid)
        tmp = min(temp_cold, humidity_humid)
        max_uptime_short = max(tmp, max_uptime_short)
        tmp = min(temp_normal, humidity_humid)
        max_uptime_short = max(tmp, max_uptime_short)
        tmp = min(temp_warm, humidity_humid)
        max_uptime_short = max(tmp, max_uptime_short)
        tmp = min(temp_freezing, humidity_normal)
        max_uptime_short = max(tmp, max_uptime_short)

        max_uptime_med = min(temp_hot, humidity_humid)
        tmp = min(temp_freezing, humidity_dry)
        max_uptime_med = max(tmp, max_uptime_med)
        tmp = min(temp_cold, humidity_normal)
        max_uptime_med = max(tmp, max_uptime_med)
        tmp = min(temp_normal, humidity_normal)
        max_uptime_med = max(tmp, max_uptime_med)
        tmp = min(temp_warm, humidity_normal)
        max_uptime_med = max(tmp, max_uptime_med)

        max_uptime_long = min(temp_hot, humidity_normal)
        tmp = min(temp_cold, humidity_dry)
        max_uptime_long = max(tmp, max_uptime_long)
        tmp = min(temp_normal, humidity_dry)
        max_uptime_long = max(tmp, max_uptime_long)
        tmp = min(temp_warm, humidity_dry)
        max_uptime_long = max(tmp, max_uptime_long)
        tmp = min(temp_hot, humidity_dry)
        max_uptime_long = max(tmp, max_uptime_long)

        res = 0
        imp = 0.0
        for i in range(100):
            if i < 50:
                uptime_short = FuzzySystem.compute_uptime_short(i)
                uptime_med = FuzzySystem.compute_uptime_medium(i)
                if uptime_short > uptime_med and max_uptime_short > 0.0:
                    if uptime_short > max_uptime_short:
                        res += max_uptime_short * i
                        imp += max_uptime_short
                    else:
                        res += uptime_short * i
                        imp += uptime_short
                elif max_uptime_med > 0.0:
                    if uptime_med > max_uptime_med:
                        res += max_uptime_med * i
                        imp += max_uptime_med
                    else:
                        res += uptime_med * i
                        imp += uptime_med
            else:
                uptime_med = FuzzySystem.compute_uptime_medium(i)
                uptime_long = FuzzySystem.compute_uptime_long(i)
                if uptime_med > uptime_long and max_uptime_med > 0.0:
                    if uptime_med > max_uptime_med:
                        res += max_uptime_med * i
                        imp += max_uptime_med
                    else:
                        res += uptime_med * i
                        imp += uptime_med
                elif max_uptime_long > 0.0:
                    if uptime_long > max_uptime_long:
                        if uptime_long > max_uptime_long:
                            res += max_uptime_long * i
                            imp += max_uptime_long
                        else:
                            res += uptime_long * i
                            imp += uptime_long

        res /= imp
        return res
